import 'package:first_project/login_details.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _uName, _uPassword;
  bool _autovalidate = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Form(
        autovalidate: _autovalidate,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                margin: const EdgeInsets.all(10),
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                          icon: Icon(Icons.person),
                          labelText: 'User Name',
                        ),
                        onChanged: (v) {
                          this._uName = v;
                        },
                        validator: (String value) {
                          return (value.length == 0)
                              ? 'Please Enter User Name'
                              : null;
                        },
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          icon: Icon(Icons.person),
                          labelText: 'Password',
                        ),
                        onChanged: (password) {
                          this._uPassword = password;
                        },
                        validator: (String value) {
                          return (value.length == 0)
                              ? 'Please Enter Password'
                              : null;
                        },
                      )
                    ],
                  ),
                ),
              ),
              FlatButton(
                color: Colors.blue,
                onPressed: () {
                  if (_uName.length == 0 || _uPassword.length == 0) {
                    setState(() {
                      _autovalidate = true;
                    });
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginDetails(this._uName)),
                    );
                  }
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
